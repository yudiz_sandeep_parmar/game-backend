"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("../utils/index");
exports.default = (socket, next) => __awaiter(void 0, void 0, void 0, function* () {
    const { token } = socket.handshake.auth;
    if (!token) {
        throw new Error(index_1.messages.err_unauthorized);
    }
    try {
        _.verifyToken(token);
    }
    catch (e) {
        log.red(e);
        throw new Error(index_1.messages.access_denied);
    }
    next();
});
