"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const mongoose = new mongoose_1.Mongoose();
const User = new mongoose.Schema({
    sEmail: { type: String, default: '' },
    sMobile: { type: String, default: '' },
    sUserName: { type: String, default: '' },
    sFullName: { type: String, default: '' },
    sPassword: { type: String },
    dDob: Date,
    sToken: String,
    sVerificationToken: String,
    sRootSocket: String,
    nBotLevel: Number,
    isEmailVerified: { type: Boolean, default: false },
    aRummyTable: [mongoose.Schema.Types.ObjectId],
    eGender: {
        type: String,
        enum: ['male', 'female', 'unspecified'],
        default: 'unspecified'
    },
    eUserType: {
        type: String,
        enum: ['user', 'admin', 'bot', 'ubot'],
        default: 'user'
    },
    eStatus: {
        type: String,
        enum: ['y', 'n', 'd'],
        default: 'y'
    }
}, { timestamps: { createdAt: 'dCreatedDate', updatedAt: 'dUpdatedDate' } });
exports.default = mongoose.model('users', User);
