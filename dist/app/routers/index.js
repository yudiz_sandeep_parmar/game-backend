"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const node_http_1 = __importDefault(require("node:http"));
const cors_1 = __importDefault(require("cors"));
const body_parser_1 = __importDefault(require("body-parser"));
const helmet_1 = __importDefault(require("helmet"));
const morgan_1 = __importDefault(require("morgan"));
class App {
    constructor() {
        this.app = (0, express_1.default)();
        this.httpServer = node_http_1.default.createServer(this.app);
        this.corsOptions = {
            origin: '*',
            methods: ['GET', 'POST', 'PUT', 'PATCH'],
            allowedHeaders: ['Content-Type', 'Authorization'],
            exposedHeaders: ['*']
        };
    }
    initialize() {
        this.setupMiddleware();
        this.setupServer();
    }
    setupMiddleware() {
        this.app.use((0, cors_1.default)(this.corsOptions));
        this.app.use((0, helmet_1.default)());
        this.app.use(express_1.default.json());
        this.app.use(body_parser_1.default.urlencoded({ extended: true }));
        this.app.use((0, morgan_1.default)('dev', { skip: (req) => req.path === '/ping' || req.path === '/favicon.ico' }));
        this.app.use(this.routeConfig);
        this.app.use('*', this.routeHandler);
        this.app.use(this.logErrors);
        this.app.use(this.errorHandler);
    }
    setupServer() {
        this.httpServer = new node_http_1.default.Server(this.app);
        this.httpServer.timeout = 10000;
        this.httpServer.listen(process.env.PORT, () => log.green(`Spinning on ${process.env.PORT}!`));
    }
    routeConfig(req, res, next) {
        if (req.path === '/ping')
            return res.status(200).send({});
        res.reply = ({ code, message }, data = {}, header = undefined) => {
            res.status(code).header(header).json({ message, data });
        };
        next();
    }
    routeHandler(req, res) {
        res.status(404);
        res.send({ message: 'Route not found' });
    }
    logErrors(err, req, res, next) {
        log.red(`${req.method} ${req.url}`);
        log.red('body -> ', req.body);
        log.red(err.stack);
        return next(err);
    }
    errorHandler(err, req, res, next) {
        res.status(500);
        res.send({ message: err });
    }
}
exports.default = new App();
