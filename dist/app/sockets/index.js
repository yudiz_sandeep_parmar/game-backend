"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const socket_io_1 = require("socket.io");
const Root_1 = __importDefault(require("./lib/Root"));
class Socket {
    constructor() {
        this.options = {
            pingInterval: 10000,
            pingTimeout: 8000,
            maxHttpBufferSize: 1e8,
            allowUpgrades: true,
            perMessageDeflate: false,
            serveClient: true,
            cookie: false,
            transports: ['websocket'],
            path: '/socket.io/',
            connectTimeout: 45000,
            allowEIO3: true,
            cors: {
                origin: '*:*',
                methods: ['GET', 'POST'],
                credentials: false
            }
        };
    }
    initialize(httpServer) {
        const io = new socket_io_1.Server(httpServer, this.options);
        global.io = io;
        Root_1.default.initialize();
        log.blue('Socket.io initialized 🔌');
    }
}
exports.default = new Socket();
