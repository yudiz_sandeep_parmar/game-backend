"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class PlayerSocket {
    constructor(socket) {
        this.socket = socket;
        this.user_id = socket.data.user_id;
        this.token = socket.data.token;
        this.setEventListeners();
        log.white(`${this.user_id} connected with socketId: ${socket.id}`);
    }
    setEventListeners() { }
}
exports.default = PlayerSocket;
