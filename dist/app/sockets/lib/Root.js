"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Player_1 = __importDefault(require("./Player"));
const index_1 = require("../../utils/index");
class RootSocket {
    initialize() {
        this.setEventListeners();
    }
    setEventListeners() {
        global.io.use((socket, next) => __awaiter(this, void 0, void 0, function* () { return yield this.middleware(socket, next); }));
        global.io.on('connection', (socket) => new Player_1.default(socket));
    }
    middleware(socket, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const { token } = socket.handshake.auth;
            if (!token) {
                throw new Error(index_1.messages.err_unauthorized);
            }
            try {
                _.verifyToken(token);
            }
            catch (e) {
                log.red(e);
                throw new Error(index_1.messages.access_denied);
            }
            next();
        });
    }
}
exports.default = new RootSocket();
