"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.mongoose = exports.mongoClient = void 0;
const mongoose_1 = require("mongoose");
const mongoose = new mongoose_1.Mongoose();
exports.mongoose = mongoose;
class MongoClient {
    static mongify(id) {
        return new mongoose.Types.ObjectId(id);
    }
    initialize() {
        mongoose
            .connect(process.env.MONGO_URL)
            .then(() => log.yellow('Database connected!'))
            .catch((error) => {
            throw error;
        });
    }
}
MongoClient.id = mongoose.Types.ObjectId;
const mongoClient = new MongoClient();
exports.mongoClient = mongoClient;
