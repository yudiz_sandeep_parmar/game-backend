"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ioredis_1 = __importDefault(require("ioredis"));
class RedisClient {
    constructor() {
        this.options = {
            host: 'localhost',
            port: parseInt(process.env.REDIS_PORT)
        };
    }
    initialize() {
        try {
            this.redisClient = new ioredis_1.default(this.options);
            this.publisher = new ioredis_1.default(this.options);
            this.subscriber = new ioredis_1.default(this.options);
            this.redisClient.on('ready', () => log.green('Redis connected!'));
            this.redisClient.on('error', log.red);
        }
        catch (error) {
            throw new Error(error);
        }
    }
}
exports.default = new RedisClient();
