"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const helper_1 = __importDefault(require("./lib/helper"));
const log_1 = __importDefault(require("./lib/log"));
global._ = helper_1.default;
global.log = log_1.default;
