"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const bcryptjs_1 = __importDefault(require("bcryptjs"));
const helper = {
    verifyToken: function (token) {
        try {
            return jsonwebtoken_1.default.verify(token, process.env.TOKEN, function (err, decoded) {
                return err != null ? err.message : decoded;
            });
        }
        catch (error) {
            return error ? error.message : error;
        }
    },
    validateEmail: (email) => __awaiter(void 0, void 0, void 0, function* () {
        const sRegexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        return !(email.match(sRegexEmail) == null);
    }),
    pick: (object, keys) => {
        return keys.reduce((obj, key) => {
            if (object === null || object === void 0 ? void 0 : object.hasOwnProperty(key)) {
                obj[key] = object[key];
            }
            return obj;
        }, {});
    },
    hashPassword: (password) => __awaiter(void 0, void 0, void 0, function* () {
        return bcryptjs_1.default.hash(password, 5);
    }),
    comparePassword: (password, hashPassword) => __awaiter(void 0, void 0, void 0, function* () {
        return bcryptjs_1.default.compare(password, hashPassword);
    }),
    validateMobile: (mobile) => {
        return mobile.match(/^\d{10}$/) == null;
    }
};
exports.default = helper;
