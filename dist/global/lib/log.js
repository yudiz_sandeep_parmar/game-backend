"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function prepare(color, ...logs) {
    const aLogs = [];
    for (const element of logs) {
        aLogs.push(`\x1b${color}`);
        aLogs.push(typeof element === 'object' ? JSON.stringify(element, null, 2) : element);
    }
    aLogs.push('\x1b[0m');
    console.log(...aLogs);
}
const logger = {
    black: (...logs) => prepare('[30m', ...logs),
    red: (...logs) => prepare('[31m', ...logs),
    green: (...logs) => prepare('[32m', ...logs),
    yellow: (...logs) => prepare('[33m', ...logs),
    blue: (...logs) => prepare('[34m', ...logs),
    magenta: (...logs) => prepare('[35m', ...logs),
    cyan: (...logs) => prepare('[36m', ...logs),
    white: (...logs) => prepare('[37m', ...logs)
};
exports.default = logger;
