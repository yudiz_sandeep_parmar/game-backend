"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv/config");
const index_1 = __importDefault(require("./app/routers/index"));
const index_2 = __importDefault(require("./app/sockets/index"));
const index_3 = require("./app/utils/index");
require('./global/');
index_3.RedisClient.initialize();
index_3.mongoClient.initialize();
index_1.default.initialize();
index_2.default.initialize(index_1.default.httpServer);
