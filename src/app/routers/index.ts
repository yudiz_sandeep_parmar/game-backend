import express, { Express, Request, Response, NextFunction } from 'express';
import http, { Server as HttpServer } from 'node:http';
import cors from 'cors';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import morgan from 'morgan';
// const routers = [require('./model-routes-services/user/auth/routes')];

interface MetaData {
  code: number;
  message: string;
}

class App {
  app: Express;
  httpServer: HttpServer;
  corsOptions: any;
  constructor() {
    this.app = express();
    this.httpServer = http.createServer(this.app);
    this.corsOptions = {
      origin: '*',
      methods: ['GET', 'POST', 'PUT', 'PATCH'],
      allowedHeaders: ['Content-Type', 'Authorization'],
      exposedHeaders: ['*']
    };
  }

  initialize() {
    this.setupMiddleware();
    this.setupServer();
  }

  setupMiddleware() {
    this.app.use(cors(this.corsOptions));
    this.app.use(helmet());
    this.app.use(express.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(morgan('dev', { skip: (req) => req.path === '/ping' || req.path === '/favicon.ico' }));
    this.app.use(this.routeConfig);
    // this.app.use('/api', routers);
    this.app.use('*', this.routeHandler);
    this.app.use(this.logErrors);
    this.app.use(this.errorHandler);
  }

  setupServer() {
    this.httpServer = new http.Server(this.app);
    this.httpServer.timeout = 10000;
    this.httpServer.listen(process.env.PORT, () => log.green(`Spinning on ${process.env.PORT}!`));
  }

  routeConfig(req: Request, res: any, next: NextFunction) {
    if (req.path === '/ping') return res.status(200).send({});
    res.reply = ({ code, message }: MetaData, data = {}, header = undefined) => {
      res.status(code).header(header).json({ message, data });
    };
    next();
  }

  routeHandler(req: Request, res: Response) {
    res.status(404);
    res.send({ message: 'Route not found' });
  }

  logErrors(err: Error, req: Request, res: Response, next: NextFunction) {
    log.red(`${req.method} ${req.url}`);
    log.red('body -> ', req.body);
    log.red(err.stack);
    return next(err);
  }

  errorHandler(err: Error, req: Request, res: Response, next: NextFunction) {
    res.status(500);
    res.send({ message: err });
  }
}

export default new App();
