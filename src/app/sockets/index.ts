import { Server, ServerOptions } from 'socket.io';
import { Server as HttpServer } from 'http';
import rootSocket from './lib/Root';
class Socket {
  options: Partial<ServerOptions> | undefined;

  constructor() {
    this.options = {
      pingInterval: 10000, // - default 25000
      pingTimeout: 8000, // - default 20000
      maxHttpBufferSize: 1e8, // - default 1e8 -> 1 MB
      allowUpgrades: true,
      perMessageDeflate: false,
      serveClient: true,
      cookie: false,
      transports: ['websocket'],
      path: '/socket.io/',
      connectTimeout: 45000, // - ms to wait before rejecting handshake
      allowEIO3: true,
      cors: {
        origin: '*:*',
        methods: ['GET', 'POST'],
        credentials: false
      }
    };
  }

  initialize(httpServer: HttpServer) {
    const io = new Server(httpServer, this.options);
    global.io = io;
    rootSocket.initialize();
    log.blue('Socket.io initialized 🔌');
  }
}

export default new Socket();
