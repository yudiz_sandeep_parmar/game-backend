import { Socket } from 'socket.io';

class PlayerSocket {
  socket: Socket;
  user_id: string;
  token: string;

  constructor(socket: Socket) {
    this.socket = socket;

    this.user_id = socket.data.user_id;
    this.token = socket.data.token;
    this.setEventListeners();
    log.white(`${this.user_id} connected with socketId: ${socket.id}`);
  }

  private setEventListeners() {}
}

export default PlayerSocket;
