/* eslint-disable @typescript-eslint/no-misused-promises */
import { Socket } from 'socket.io';
import { ExtendedError } from 'socket.io/dist/namespace';
import PlayerSocket from './Player';
import { messages } from '../../utils/index';
declare type iNext = () => void;
class RootSocket {
  initialize() {
    this.setEventListeners();
  }

  private setEventListeners() {
    global.io.use(async (socket, next) => await this.middleware(socket, next));
    global.io.on('connection', (socket: Socket) => new PlayerSocket(socket));
  }

  private async middleware(socket: Socket, next: iNext) {
    const { token } = socket.handshake.auth;

    if (!token) {
      throw new Error(messages.err_unauthorized);
    }
    try {
      _.verifyToken(token);
    } catch (e) {
      log.red(e);
      throw new Error(messages.access_denied);
    }

    //   const user = await User.findOneAndUpdate({ 'sessions.token': token }, { $set: { 'sessions.$.socketId': socket.id } }, { fields: { _id: 1, email: 1, sessions: 1 }, new: true });
    // eslint-disable-next-line no-param-reassign
    //   socket.user = user;
    next();
  }
}

export default new RootSocket();
