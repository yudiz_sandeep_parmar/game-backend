import { Mongoose } from 'mongoose';
const mongoose = new Mongoose();
class MongoClient {
  static id = mongoose.Types.ObjectId;

  static mongify(id: string) {
    return new mongoose.Types.ObjectId(id);
  }

  initialize() {
    mongoose
      .connect(process.env.MONGO_URL!)
      .then(() => log.yellow('Database connected!'))
      .catch((error: any) => {
        throw error;
      });
  }
}

const mongoClient = new MongoClient();

export { mongoClient, mongoose };
