import Redis, { RedisOptions } from 'ioredis';

class RedisClient {
  public options: RedisOptions;

  public redisClient: any;

  public publisher: any;

  public subscriber: any;

  constructor() {
    this.options = {
      host: 'localhost',
      port: parseInt(process.env.REDIS_PORT as string)
    };
  }

  initialize() {
    try {
      this.redisClient = new Redis(this.options);
      this.publisher = new Redis(this.options);
      this.subscriber = new Redis(this.options);

      this.redisClient.on('ready', () => log.green('Redis connected!'));
      this.redisClient.on('error', log.red);

      // await Promise.all([this.client.connect(), this.publisher.connect(), this.subscriber.connect()]);
    } catch (error: any) {
      throw new Error(error);
    }
  }
}
export default new RedisClient();
