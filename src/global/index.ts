/* eslint-disable no-var */
import { Server } from 'socket.io';
import helper from './lib/helper';
import logger from './lib/log';
interface ServerToClientEvents {
  noArg: () => void;
  basicEmit: (a: number, b: string, c: Buffer) => void;
  withAck: (d: string, callback: (e: number) => void) => void;
}

interface ClientToServerEvents {
  hello: () => void;
}

interface InterServerEvents {
  ping: () => void;
}

declare global {
  var _: typeof helper;
  var log: typeof logger;
  var io: Server<ClientToServerEvents, ServerToClientEvents, InterServerEvents, any>;
}

global._ = helper;
global.log = logger;

export {};
