/* eslint-disable import/no-unresolved */
/* eslint-disable no-prototype-builtins */
import jwt from 'jsonwebtoken';
import bcryptjs from 'bcryptjs';
// import { messages, status, jsonStatus } from './api.response';
// import bcryptjs from 'bcryptjs';

const helper = {
  verifyToken: function (token: string) {
    try {
      return jwt.verify(token, process.env.TOKEN as string, function (err, decoded) {
        return err != null ? err.message : decoded; // return true if token expired
      });
    } catch (error: any) {
      // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
      return error ? error.message : error;
    }
  },
  // _.catchError = (name, error, req, res) => {
  //   _.handleCatchError(error);
  //   return res.status(status.InternalServerError).jsonp({
  //     status: jsonStatus.InternalServerError,
  //     message: messages.error
  //   });
  // };

  // _.handleCatchError = (error) => {
  //   console.log('**********ERROR***********', error);
  // };
  validateEmail: async (email: string) => {
    const sRegexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return !(email.match(sRegexEmail) == null);
  },
  pick: (object: any, keys: string[]) => {
    return keys.reduce((obj: any, key: any) => {
      // eslint-disable-next-line @typescript-eslint/strict-boolean-expressions
      if (object?.hasOwnProperty(key)) {
        obj[key] = object[key];
      }
      return obj;
    }, {});
  },

  // _.chunk = (array:[], size) => {
  //   const result = [];
  //   for (let i = 0; i < array.length; i += size) {
  //     const chunks = array.slice(i, i + size);
  //     result.push(chunks);
  //   }
  //   return result;
  // };

  hashPassword: async (password: string) => {
    return bcryptjs.hash(password, 5);
  },

  comparePassword: async (password: string, hashPassword: string) => {
    return bcryptjs.compare(password, hashPassword);
  },

  validateMobile: (mobile: string) => {
    return mobile.match(/^\d{10}$/) == null;
  }
};

export default helper;
