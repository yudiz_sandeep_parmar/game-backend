// interface Logger {
//   black: (...logs: any) => void;
//   red: (...logs: any) => void;
//   green: (...logs: any) => void;
//   yellow: (...logs: any) => void;
//   blue: (...logs: any) => void;
//   magenta: (...logs: any) => void;
//   cyan: (...logs: any) => void;
//   white: (...logs: any) => void;
// }

function prepare(color: string, ...logs: any) {
  const aLogs = [];
  // eslint-disable-next-line no-restricted-syntax
  for (const element of logs) {
    aLogs.push(`\x1b${color}`);
    aLogs.push(typeof element === 'object' ? JSON.stringify(element, null, 2) : element);
  }
  aLogs.push('\x1b[0m');
  console.log(...aLogs);
}
const logger = {
  black: (...logs: any) => prepare('[30m', ...logs),
  red: (...logs: any) => prepare('[31m', ...logs),
  green: (...logs: any) => prepare('[32m', ...logs),
  yellow: (...logs: any) => prepare('[33m', ...logs),
  blue: (...logs: any) => prepare('[34m', ...logs),
  magenta: (...logs: any) => prepare('[35m', ...logs),
  cyan: (...logs: any) => prepare('[36m', ...logs),
  white: (...logs: any) => prepare('[37m', ...logs)
};

export default logger;
