import 'dotenv/config';
import app from './app/routers/index';
import socket from './app/sockets/index';
import { mongoClient, RedisClient } from './app/utils/index';
require('./global/');

RedisClient.initialize();
mongoClient.initialize();
app.initialize();
socket.initialize(app.httpServer);
